//
//  UGUser.swift
//  UGAPIApp
//
//  Created by Administrator on 5/09/2014.
//  Copyright (c) 2014 Administrator. All rights reserved.
//

import Foundation

class UGUser: NSObject {
    
    var username : String
    var email: String
    var uuid: String
    var picture: String
    
    init(username:String , email: String , uuid: String, picture: String)
    {
        self.username = username
        self.email = email
        self.uuid = uuid
        self.picture = picture
    }
    
}
